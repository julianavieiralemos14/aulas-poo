//Escreva um programa TypeScript que imprima todos os números primos de 1 a 53 usando a função while.

namespace exercicio_1
{
    let i: number;
    i = 1;
    while(i <= 53)
    {
        console.log(i);
        i++;
    
    }

    let i1: number;
    i1 = 0;
    
    do {
        console.log(`Esse número é impar: ${i1}`);
        i1++;
    } while(i1 <= 53);

}

//errado

//Crie um array com 5 números. Em seguida, use um loop for para iterar 
//sobre o array e exibir a soma de todos os números.

namespace exercicio_1
{
    let numeros: number[] = [2, 4, 6, 8, 10];

    let j = 0;

    let desc: string[] = ["Nota 1", "Nota 2", "Nota 3", "Nota 4", "Nota 5"];

    let qtdNotas: number = numeros.length;

    let somaNotas: number = 0;
    for (let i = 0; i < numeros.length; i++)
    {
        somaNotas = somaNotas + numeros[i];
    }

    console.log(somaNotas);
        
}


//Crie um vetor chamado "alunos" contendo três objetos, cada um representando um aluno com as seguintes 
//propriedades: "aluno"(string) "idade"(number) "notas"(array de número).
//Preencha o vetorcom informações ficticías.
//Em seguida, percorra o vetor utilizando a função "forEach" e para cada aluno, calcule a média das notas 
//e imprima o resultado na tela, juntamente com o nome e a idade do aluno.

    interface Aluno
    {
        nome:string;
        idade:number;
        notas:number[]
    }

    
namespace exercicio_6 {

    const alunos: Aluno[] = [
        {nome: "Aluno 1", idade: 20, notas:[4, 7, 8]},
        {nome: "Aluno 2", idade: 23, notas:[2, 5, 10]},
        {nome: "Aluno 3", idade: 49, notas:[10, 9, 1]},
        {nome: "Aluno 4", idade: 18, notas:[3, 4, 2]},
        {nome: "Aluno 5", idade: 33, notas:[1, 10, 5]}
    ]

    alunos.forEach((aluno) => 
    {
        console.log("-----------------------")
        console.log((aluno.notas[0] + aluno.notas[1] + aluno.notas[2]) / 3 );
    })
}

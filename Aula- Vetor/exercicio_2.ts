//Crie um array com 3 nomes de frutas. Em seguida, use um loop while
//para iterar sobre o array e exibir cada fruta em uma linha separada.

namespace exercicio_2
{
    let frutas: string[] = ["morango", "uva", "banana"];
    
    //console.log(frutas;
    //console.log(frutas[morango]);
    //console.log(frutas[uva]);
    //console.log(frutas[banana]);

    let i = 0;
    while(i < frutas.length)
    {
        console.log(frutas[i]);
        i++;
    }
    
}
//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor.
// Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.

namespace exercicio_3
{
    let livros: any[] = 
    [
        {titulo: "A cinco passos de você",autor: " Rachael Lippincott",},
        {titulo: "É assim que acaba", autor: " Colleen Hoover",}, 
        {titulo: "Mil beijos de um garoto", autor: "Tillie Cole"},
        {titulo: "Cartas secretas jamais enviadas", autor: "Emily Trunko"}
    ]

    let result = livros.map(function (res) {return res.titulo});

    console.log(result);
}

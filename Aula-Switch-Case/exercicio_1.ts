namespace exercico_1 {
  let conhecimento: number;
  conhecimento = 7;

  switch (conhecimento) {
    case 10:
      console.log("Avançado!");
      break;
    case 7:
      console.log("Mediano!");
      break;
    case 4:
      console.log("Iniciante!");
      break;
    case 0:
      console.log("Pessímo!");
      break;
    default:
      console.log("Não é uma opção válida!");
  }
}

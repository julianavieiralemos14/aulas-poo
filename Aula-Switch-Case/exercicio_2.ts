namespace exercico_2 {
  let cor: string = "Roxo";

  switch (cor) {
    case "Roxo":
      console.log("Poder!");
      break;
    case "Azul":
      console.log("Confiança!");
      break;
    case "Vermelho":
      console.log("Desejo!");
      break;
    case "Verde":
      console.log("Orgulho!");
      break;
    case "Branco":
      console.log("Pureza!");
      break;
    case "Amarelo":
      console.log("Juventude!");
      break;
    case "Cinza":
      console.log("Depressão!");
      break;
    case "Laranja":
      console.log("Aventura");
      break;
    case "Rosa":
      console.log("Fragilidade!");
      break;
    case "Marrom":
      console.log("Conforto!");
      break;
    default:
      console.log("Que cor é essa??");
  }
}

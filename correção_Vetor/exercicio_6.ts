interface Aluno
    {
        nome:string;
        idade:number;
        notas:number[]
    }

    
namespace exercicio_6 {

    const alunos: Aluno[] = [
        {nome: "Aluno 1", idade: 20, notas:[4, 7, 8]},
        {nome: "Aluno 2", idade: 23, notas:[2, 5, 10]},
        {nome: "Aluno 3", idade: 49, notas:[10, 9, 1]},
        {nome: "Aluno 4", idade: 18, notas:[3, 4, 2]},
        {nome: "Aluno 5", idade: 33, notas:[1, 10, 5]}
    ]

    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce(
            (total, nota) => {return total + nota}) / aluno.notas.length

            if (media >= 7) {
                console.log(`A media do aluno: ${aluno.nome} é igual ${media} e está aprovado`);
            }

            else {
                console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está reprovado`);
                
            }
    })
}

namespace exercicio_1 {
  //0  1  2  3  4
  let numeros: number[] = [1, 2, 3, 4, 5];
  let soma: number = 0;

  for (let i = 0; i < numeros.length; i++) {
    soma += numeros[i]; //soma = soma + numeros[i]
  }

  console.log(`O resultado da soma é: ${soma}`);

  //Criando uma interação com multiplicação
  let multi: number = 1;
  for (let i = 0; i < numeros.length; i++) {
    multi *= numeros[i];
  }

  console.log(`O resultado da multiplicação é: ${multi}`);
}

let livros: any[] = 
    [
        {titulo: "Titulo 1", autor:"Autor 1"},
        {titulo: "Titulo ab", autor:"Autor 3"},
        {titulo: "Titulo af", autor:"Autor 3"},
        {titulo: "Titulo 4", autor:"Autor 4"},
    ];

let autor3 = livros.filter((livro) => 
{
    return livro.autor == "Autor 3"
});

let titulos = autor3.map((livro) => 
{
    return livro.titulo
})
console.log(titulos);

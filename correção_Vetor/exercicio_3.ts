namespace exercicio_3
{
    /*let obj: any = 
    {
        nome:"Leonardo",
        idade: 33,
        email:"leonardosenai604@gmail.com"
    }

    console.log(obj);*/

    let livros: any[] = 
    [
        {titulo: "Titulo 1", autor:"Autor 1"},
        {titulo: "Titulo 2", autor:"Autor 2"},
        {titulo: "Titulo 3", autor:"Autor 3"},
        {titulo: "Titulo 4", autor:"Autor 4"},
    ];

    let titulos = livros.map((livro) =>
    {
        return livro.titulo
    });
    
    let autores = livros.map((livro) =>
    {
        return livro.autor
    });
    
    console.log(autores);
    console.log(titulos);
    
    
    
}